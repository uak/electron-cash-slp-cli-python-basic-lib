import re
import json
import time
import pytest
import subprocess

from ec_slp_lib import EcSLP


from test_helper import (
    params,
    rpc,
    sleep_time,
    test_address,
    load_wallet_sleep_time,
)


# create an instance of class EcSLP
ec_slp = EcSLP()


def get_ec_daemon_status(params):
    """
    Get status of the daemon, used in tests
    """
    cmd = ("daemon", "status")
    try:
        check = ec_slp.subprocessing(cmd, params)
        return json.loads(check)
    except subprocess.CalledProcessError as err:
        return err.output.strip()


def test_subprocessing():
    cmd = ("version",)
    pattern = re.compile("([0-9])+\\.([0-9])+\\.([0-9])+")
    assert pattern.match(ec_slp.subprocessing(cmd, params))


def test_start_daemon():
    ec_slp.start_daemon(params)
    time.sleep(sleep_time)

    assert get_ec_daemon_status(params)["connected"] is True


def test_check_daemon():
    assert get_ec_daemon_status(params)["connected"] is True
    assert ec_slp.check_daemon(params) is True


def test_fail_check_daemon():
    ec_slp.stop_daemon(params)
    time.sleep(1)
    assert ec_slp.check_daemon(params) is False


def test_load_wallet():
    ec_slp.start_daemon(params)
    ec_slp.load_wallet(params, params["wallet_path"])
    time.sleep(load_wallet_sleep_time)
    assert params["wallet_path"] in get_ec_daemon_status(params)["wallets"]
    assert get_ec_daemon_status(params)["wallets"][params["wallet_path"]] is True


def test_check_wallet_loaded():
    # ~ start_daemon()
    assert ec_slp.check_wallet_loaded(params, params["wallet_path"])


def test_get_config_data():
    cmd = ("getconfig", "rpcport")
    rpc_port = ec_slp.subprocessing(cmd, params).rstrip()
    assert ec_slp.get_config_data(params, "rpcport") == rpc_port


def test_set_config_data():
    new_rpc_port = 15010

    old_rpc_port = ec_slp.get_config_data(params, "rpcport")
    ec_slp.set_config_data(params, "rpcport", str(new_rpc_port))
    current_rpc_port = ec_slp.get_config_data(params, "rpcport")
    # Re set old port
    ec_slp.set_config_data(params, "rpcport", old_rpc_port)
    assert current_rpc_port == str(new_rpc_port)


# not implemented yet
# ~ def test_get_unused_slp_address():

# ~ pass


def test_get_bch_balance():
    cmd = ("getbalance", "-w", params["wallet_path"])
    balance = json.loads(ec_slp.subprocessing(cmd, params))
    assert ec_slp.get_bch_balance(rpc) == balance


def test_get_address_balance_bch():
    cmd = ("getaddressbalance", test_address)
    address_balance = json.loads(ec_slp.subprocessing(cmd, params))
    assert ec_slp.get_address_balance_bch(rpc, test_address) == address_balance


# not implemented yet
# ~ def test_get_token_balance():
# ~ pass

# not implemented yet
# ~ def test_prepare_slp_transaction():
# ~ pass


def test_stop_daemon():
    ec_slp.stop_daemon(params)
    assert get_ec_daemon_status(params) == "Daemon not running"
